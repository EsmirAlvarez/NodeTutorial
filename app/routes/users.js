import Ember from 'ember';

var url = 'https://api.github.com/users';
export default Ember.Route.extend({
    model: function(){
        return Ember.$.getJSON(url).then(function(data){
            return data.splice(0,10);
        });
    }
});
