import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('about');
  this.route('services');
  this.route('contacts');

  this.resource('posts', {path: '/posts'}, function(){
      this.route('new');
      this.route('post', {path: ':post_id'});
  })
  this.route('post');
  this.route('cars', function() {
    this.route('new');
    this.route('edit', {path: '/edit/:car_id'});
  });
  this.route('users');
});

export default Router;
