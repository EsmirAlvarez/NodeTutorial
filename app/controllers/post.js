import Ember from 'ember';

export default Ember.Controller.extend({
    actions:{
        sayHello: function(){
            alert('Hello!');
        },
        submitAction: function(){
            alert(this.get('name'));
            alert(this.get('comment'));
        }
   
    },
    title: 'My Blog Post',
    body: 'This is the body of my blog post',
    authors: ['William', 'Robert'],
    comments: [
        {
            name: 'Mike Smith',
            comment: 'Thanks....'
        },
        {
            name: 'Mike Smith',
            comment: 'Thanks....'
        },
        {
            name: 'Mike Smith',
            comment: 'Thanks....'
        },
    ]
});
